import { initSimpleApp } from '~/helpers/init_simple_app_helper';
import FeatureSettingsApp from './components/app.vue';

initSimpleApp('#js-ai-powered-features', FeatureSettingsApp);
